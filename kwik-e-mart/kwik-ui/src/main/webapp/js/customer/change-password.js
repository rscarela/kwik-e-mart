var changePassword = {
		
		verifyCurrentPassword : function(){
			if(!changePassword.isNewPasswordValid()){
				alert("Erro! Nova senha e confirma��o est�o divergentes!");
				return;
			}
			
			$.ajax({
				url : '/kwik-ui/credential/verifyPassword',
				type : 'POST',
				data : changePassword.getCurrentCredential(),
				success : function(isCurrentInformationCorrect){
					if(isCurrentInformationCorrect){
						kwik.post('change-password-form', '/credential/changePassword', '/customer/panel');
						return;
					}
					
					alert("Erro! Senha atual inv�lida.");
				}
			});
		},
		
		getCurrentCredential : function(){
			var currentCredential = new Object();
			
			currentCredential.email = $("#user-email").val();
			currentCredential.password = $("#current-password").val();
			
			return currentCredential;
		},
		
		isNewPasswordValid : function(){
			if($("#new-password").val() == $("#new-password-confirmation").val()) return true;
			return false;
		}
		
};