var customerForm = {
	
		validateFields : function(){
			$(".required").each(function(){
				if($(this).val() == "") {
					customerForm.disableSubmitButton();
					return;
				}
				customerForm.enableSubmitButton();
			});
		},
		
		disableSubmitButton : function(){
			$("#submit-customer").attr('disabled', 'disabled');
		},
		
		enableSubmitButton : function(){
			$("#submit-customer").removeAttr('disabled');
		},
		
		bind : function(){
			$(".required").blur(customerForm.validateFields);
		},
		
		init : function(){
			customerForm.bind();
		}
};

$(customerForm.init);