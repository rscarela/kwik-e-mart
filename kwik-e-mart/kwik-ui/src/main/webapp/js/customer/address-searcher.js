var addressSearcher = {
	
		findAddress : function(){
			$.ajax({
				url : '/kwik-ui/customer/load-address?zipcode=' + $("#zipcode").val(),
				type : 'GET',
				success : function(result){
					addressSearcher.fillAddress(result);
				}
			});
		},
		
		fillAddress : function(address){
			$("#street").val(address.logradouro);
			$("#neighborhood").val(address.bairro);
			$("#city").val(address.localidade);
			$("#state").val(address.uf);
		},
		
		bind : function(){
			$(".address-search").click(addressSearcher.findAddress);
		},
		
		init : function(){
			addressSearcher.bind();
		}
};

$(addressSearcher.init);