var baseURL = '/kwik-ui';

var kwik = {
		load : function(uri){
			$("#content").load(baseURL + uri);
		},
		
		href : function(uri){
			location.href = baseURL + uri;
		},
		
		post : function(form, uri, forward){
			$.ajax({
				url : baseURL + uri,
				type : "POST",
				data : $("#" + form).serialize(),
				success : function(){
					if(forward == undefined || forward == "") return;
					kwik.load(forward);
				},
				error : function(){
					
				}
			});
		},
		
		loadView : function(form, uri, method){
			$.ajax({
				url : baseURL + uri,
				type : method,
				data : $("#" + form).serialize(),
				success : function(view){
					$("#content").html(view);
				}
			});
		},
		
		login : function(form, uri){
			$.ajax({
				url : baseURL + uri,
				type : 'POST',
				data : $("#" + form).serialize(),
				success : function(result){
					kwik.href('/index');
				},
				error : function(result){
					alert("E-mail ou senha inválidos!");
				}
			});
		}
};