<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Kwik-E-Mart</title>
		
		<link rel="stylesheet" type="text/css" href="css/skin.css"/>
		
		<script type="text/javascript" src="js/jquery/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="js/kwik.js"></script>
	</head>
	
	<body>
		<div id="top" style="border: 1px solid;">
			<#include "/home/top-bar.ftl">
		</div>
		
		<div id="content" style="border: 1px solid;">
		
		</div>
		
		<div id="bottom" style="border: 1px solid;">
		</div>
		
	</body>
</html>