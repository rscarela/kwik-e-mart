Kwik E Mart
<#if auth?? && auth>
	<span class="right right-space"><a href="javascript:kwik.href('/customer/logout')">Sair</a></span>
	<span class="right right-space"><a href="javascript:kwik.load('/customer/panel')">Minha Conta</a></span>
<#else>
	<span class="right right-space"><a href="javascript:kwik.load('/customer/signup')">Cadastrar</a></span>
	<span class="right right-space"><a href="javascript:kwik.load('/customer/login')">Login</a></span>
</#if>