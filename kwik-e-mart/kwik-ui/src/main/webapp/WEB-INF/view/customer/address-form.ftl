<form id="customer-form" method="POST" action="javascript:kwik.post('customer-form', '/customer/address', '/customer/panel')" style="margin-left: 40%;">
	<label>CEP</label><br/>
	<input type="text" id="zipcode" name="zipcode" value="${customerAddress.zipcode!}"/>
	<input type="button" class="address-search" value="Buscar"/>
	<br/>
	<label>Logradouro *</label><br/>
	<input type="text" id="street" name="street" class="required" value="${customerAddress.street!}"/>
	<br/>
	<label>Bairro *</label><br/>
	<input type="text" id="neighborhood" name="neighborhood" class="required" value="${customerAddress.neighborhood!}"/>
	<br/>
	<label>N�mero *</label><br/>
	<input type="text" id="number" name="number" class="required" value="${customerAddress.number!}"/>
	<br/>
	<label>Complemento</label><br/>
	<input type="text" id="note" name="note" value="${customerAddress.note!}"/><br/>
	<label>Cidade *</label><br/>
	<input type="text" id="city" name="city" class="required" value="${customerAddress.city!}"/>
	<br/>
	<label>Estado *</label><br/>
	<input type="text" id="state" name="state" class="required" value="${customerAddress.state!}"/><br/>
	<br/>
	<input id="submit-customer" type="submit" value="Salvar" disabled/>
</form>

<script type="text/javascript" src="js/customer/form.js"></script>
<script type="text/javascript" src="js/customer/address-searcher.js"></script>