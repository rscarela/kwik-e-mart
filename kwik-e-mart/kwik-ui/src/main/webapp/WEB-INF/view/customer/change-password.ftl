<form id="change-password-form" method="POST" action="javascript:changePassword.verifyCurrentPassword()">
	<label>Senha atual</label><br/>
	<input type="password" id="current-password" name="currentPassword"/>
	<br/>
	<label>Nova senha</label><br/>
	<input type="password" id="new-password" name="password"/>
	<br/>
	<label>Confirmação da senha</label><br/>
	<input type="password" id="new-password-confirmation"/>
	<br/><br/>
	<input type="submit" value="Alterar senha"/>
</form>

<script type="text/javascript" src="js/customer/change-password.js"></script>