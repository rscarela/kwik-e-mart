<form id="customer-form" method="POST" action="javascript:kwik.post('customer-form', '/customer/signup')" style="margin-left: 45%;">
	<input type="hidden" id="id" name="id" value="${customer.id!}"/>
	<label>Nome *</label><br/>
	<input type="text" id="name" name="name" class="required" value="${customer.name!}"/>
	<br/>
	<label>Sobrenome *</label><br/>
	<input type="text" id="surname" name="surname" class="required" value="${customer.surname!}"/>
	<br/>
	<label>CPF *</label><br/>
	<input type="text" id="socialId" name="socialId" class="required" value="${customer.socialId!}"/>
	<br/>
	<label>Data Nascimento *</label><br/>
	<@birthDate/>	<br/>
	<label>Sexo</label><br/>
	<@loadGenderOptions/>
	<br/><br/>
	
	<label>Residencial</label><br/>
	<@residentialPhone/>
	<br/>
	<label>Comercial</label><br/>
	<@comercialPhone/>
	<br/>	
	<label>Celular</label><br/>
	<@cellphone/>
	<br/><br/>
	<#if !auth??>
		<label>E-mail *</label><br/>
		<input type="text" id="email" name="email" class="required"/>
		<br/>
		<label>Senha *</label><br/>
		<input type="password" id="password" name="password" class="required"/><br/>
		<br/>
	</#if>
	<input id="submit-customer" type="submit" value="Cadastrar" disabled/>
</form>
<script type="text/javascript" src="js/customer/form.js"></script>

<#macro birthDate>
	<#if customer?? && customer.birthDate??>
		<input type="text" id="birthDate" name="birthDate" class="required" value="${customer.birthDate?string("dd/MM/yyyy")!}"/>
	<#else>
		<input type="text" id="birthDate" name="birthDate" class="required"/>
	</#if>
</#macro>

<#macro loadGenderOptions>
	<#if customer.gender?exists>
		<input type="radio" name="gender" value="FEMALE"/> Feminino
		<input type="radio" name="gender" value="MALE" checked/> Masculino
	<#else>
		<input type="radio" name="gender" value="FEMALE" checked> Feminino
		<input type="radio" name="gender" value="MALE"> Masculino
	</#if>
</#macro>

<#macro residentialPhone>
	<#if (customer?? && customer.customerPhone??)>
		<input type="text" id="residential" name="residential" value="${customer.customerPhone.residential!}"/>
	<#else>
		<input type="text" id="residential" name="residential"/>
	</#if>
</#macro>

<#macro comercialPhone>
	<#if (customer?? && customer.customerPhone??)>
		<input type="text" id="comercial" name="comercial" value="${customer.customerPhone.comercial!}"/>
	<#else>
		<input type="text" id="comercial" name="comercial"/>
	</#if>
</#macro>

<#macro cellphone>
	<#if (customer?? && customer.customerPhone??)>
		<input type="text" id="cellphone" name="cellphone" value="${customer.customerPhone.cellphone!}"/>
	<#else>
		<input type="text" id="cellphone" name="cellphone"/>
	</#if>
</#macro>