<div id="customer-menu" style="width : 15%; height: 100%; border: 1px solid" class="left">
	<span style="margin-left: 35%;">Op��es</span><br/><br/>
	
	<span class="left-space"><a href="javascript:customerPanel.loadPanelContent('/customer/pre-signup')">Editar meus dados</a></span><br/>
	<span class="left-space"><a href="javascript:customerPanel.loadPanelContent('/customer/address')">Editar meu endere�o</a></span>
	<span class="left-space"><a href="javascript:customerPanel.loadPanelContent('/customer/change-password')">Alterar senha</a></span>
</div>

<div id="customer-panel-content" class="right" style="width:84%;">
	<span>Bem vindo a sua p�gina pessoal</span><br/>
	<p>
		Aqui voc� pode atualizar suas informa��es e alterar sua senha.<br/>
		
		Lembre-se tamb�m de cadastrar um endere�o para realizar suas compras.<br/>
		N�o se esque�a de mante-lo sempre atualizado.
	</p>
</div>

<script type="text/javascript" src="js/customer/panel.js"></script>