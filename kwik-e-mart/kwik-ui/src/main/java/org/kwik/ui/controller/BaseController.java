package org.kwik.ui.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Renan Scarela - 13/08/2013
 *
 */
public class BaseController {
	
	@Autowired private HttpServletRequest request;
	
	public HttpSession getSession(){
		return request.getSession();
	}
	
	public boolean getAuth(){
		return (boolean) getSession().getAttribute("auth");
	}
	
	public void redirect(HttpServletResponse response, String uri) throws IOException{
		response.sendRedirect(uri);
	}
	
}
