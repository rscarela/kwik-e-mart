package org.kwik.ui.controller;

import org.kwik.customer.business.interfaces.ICustomerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController extends BaseController {
	
	@Autowired private ICustomerManager customerManager;
	
	@RequestMapping(value="/index")
	public String index(){
		return "index";
	}
	
}
