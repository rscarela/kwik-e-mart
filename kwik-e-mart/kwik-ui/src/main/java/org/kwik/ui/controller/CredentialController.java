package org.kwik.ui.controller;

import org.kwik.customer.Credential;
import org.kwik.customer.Customer;
import org.kwik.customer.business.interfaces.ICustomerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Renan Scarela - 15/08/2013
 *
 */

@Controller
@RequestMapping(value="/credential")
public class CredentialController extends BaseController {
	
	@Autowired private ICustomerManager customerManager;
	
	@RequestMapping(value="/verifyPassword")
	public @ResponseBody boolean checkCurrentCredential(Credential credential){
		credential.setEmail(getCustomerEmail());
		return customerManager.forCredential(credential).doesExists();
	}
	
	@RequestMapping(value="/changePassword")
	public @ResponseBody void changePassword(Credential credential){
		credential.setEmail(getCustomerEmail());
		customerManager.forCustomer(getCustomerFromSession()).createNew(credential);
	}
	
	private String getCustomerEmail(){
		return (String) getSession().getAttribute("customerEmail");
	}
	
	private Customer getCustomerFromSession(){
		return (Customer) getSession().getAttribute("customer");
	}
	
}
