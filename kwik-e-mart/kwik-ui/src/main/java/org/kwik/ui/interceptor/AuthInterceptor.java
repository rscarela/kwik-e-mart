package org.kwik.ui.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * @author Renan Scarela - 16/08/2013
 *
 */
public class AuthInterceptor extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
		if(!doesRequestRequiredAuthentication(request)
				|| isUserAthenticated(request.getSession())) return true;
		
		redirectToAuthenticationError(request, response);
		return false;
	}
	
	private boolean isUserAthenticated(HttpSession session){
		return session.getAttribute("auth") == null ? false : (Boolean) session.getAttribute("auth");
	}
	
	private boolean doesRequestRequiredAuthentication(HttpServletRequest request){
		for(String excluedURI : this.getURIsWhichNeedAuthentication()){
			if(request.getRequestURI().equals(excluedURI)) return true;
		}
		return false;
	}
	
	private String[] getURIsWhichNeedAuthentication(){
		String[] authURIs = {
				"/kwik-ui/customer/address",
				"/kwik-ui/customer/panel",
				"/kwik-ui/customer/change-password",
				"/kwik-ui/credential/verifyPassword"
		};
		return authURIs;
	}
	
	private void redirectToAuthenticationError(HttpServletRequest request, HttpServletResponse response) throws IOException{
		response.setStatus(403);
	}
	
}
