package org.kwik.ui.consumer;

import java.io.IOException;
import java.net.MalformedURLException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.kwik.customer.util.http.WebserviceConsumer;
import org.kwik.ui.controller.view.BrazilianAddress;

/**
 * @author Renan Scarela - 15/08/2013
 *
 */
public class ZipcodeConsumer {
	
	private static final String URL = "http://cep.correiocontrol.com.br/{zipcode}.json";
	
	public BrazilianAddress getAddressBy(String zipcode) throws JsonParseException, JsonMappingException, MalformedURLException, IOException{
		String url = URL.replace("{zipcode}", zipcode);
		return (BrazilianAddress) new WebserviceConsumer().consume(url).doGet().getResult().asObject(BrazilianAddress.class);
	}
	
}
