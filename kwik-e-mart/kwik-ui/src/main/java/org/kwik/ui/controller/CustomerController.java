package org.kwik.ui.controller;

import java.io.IOException;
import java.security.InvalidParameterException;

import javax.servlet.http.HttpServletResponse;

import org.kwik.customer.Credential;
import org.kwik.customer.Customer;
import org.kwik.customer.CustomerAddress;
import org.kwik.customer.business.interfaces.ICustomerManager;
import org.kwik.ui.consumer.ZipcodeConsumer;
import org.kwik.ui.controller.form.CustomerForm;
import org.kwik.ui.controller.view.BrazilianAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Renan Scarela - 12/08/2013
 *
 */

@Controller
@RequestMapping(value="/customer")
public class CustomerController extends BaseController {
	
	@Autowired private ICustomerManager customerManager;
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String loadLogin(){
		return "/customer/login";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public @ResponseBody void login(@ModelAttribute Credential credential, HttpServletResponse response) throws IOException{
		Customer customer = customerManager.forCredential(credential).getCustomer();
		executeLogin(customer, credential);
		redirect(response, "../index");
	}
	
	@RequestMapping(value="/logout")
	public @ResponseBody void logout(HttpServletResponse response) throws IOException{
		getSession().removeAttribute("customer");
		getSession().removeAttribute("email");
		getSession().removeAttribute("auth");
		redirect(response, "../index");
	}
	
	@RequestMapping(value="/signup")
	public ModelAndView loadSignup(@ModelAttribute Customer customer){
		ModelAndView view = new ModelAndView("/customer/form");
		
		if(customer != null) view.addObject("customer", customer);
		
		return view;
	}
	
	@RequestMapping(value="/pre-signup", method=RequestMethod.POST)
	public ModelAndView loadPreSignup(@ModelAttribute Customer customer){
		return loadSignup(customer);
	}
	
	@RequestMapping(value="/pre-signup", method=RequestMethod.GET)
	public ModelAndView loadPreSignupWithSessionCustomer(){
		return loadSignup((Customer) getSession().getAttribute("customer"));
	}
	
	@RequestMapping(value="/signup", method=RequestMethod.POST)
	public @ResponseBody void signup(@ModelAttribute CustomerForm form) throws IOException{
		Customer customer = form.getCustomer();

		if(form.getId() == null){
			customerManager.forCustomer(customer).createNew(form.getCredential());
			executeLogin(customer, form.getCredential());
			return;
		}
		
		customerManager.forCustomer(customer).save();
		updateCustomerInSession(customer);
		return;
	}
	
	@RequestMapping(value="/address", method=RequestMethod.GET)
	public ModelAndView loadAddressForm(){
		ModelAndView view = new ModelAndView("/customer/address-form");
		
		view.addObject("customerAddress", loadCustomerAddress());
		
		return view;
	}
	
	@RequestMapping(value="/address", method=RequestMethod.POST)
	public @ResponseBody void saveCustomerAddress(@ModelAttribute CustomerAddress customerAddress){
		customerManager.forCustomer(getCustomerFromSession()).updateAddress(customerAddress);
	}
	
	@RequestMapping(value="/load-address")
	public @ResponseBody BrazilianAddress getAddressByZipcode(@RequestParam String zipcode) {
		try {
			return new ZipcodeConsumer().getAddressBy(zipcode);
		} catch (Exception e) {
			return new BrazilianAddress();
		}
	}
	
	private CustomerAddress loadCustomerAddress(){
		CustomerAddress customerAdress = customerManager.forCustomer(getCustomerFromSession()).getAddress();
		
		if(customerAdress == null) return new CustomerAddress();
		
		return customerAdress;
	}
	
	@RequestMapping(value="/panel")
	public String panel(){
		return "/customer/panel";
	}
	
	@RequestMapping(value="/change-password")
	public String changePassword(){
		return "/customer/change-password";
	}
	
	private void executeLogin(Customer customer, Credential credential){
		if(customer == null) throw new InvalidParameterException();
		
		getSession().setAttribute("customer", customer);
		getSession().setAttribute("customerEmail", credential.getEmail());
		getSession().setAttribute("auth", true);
	}
	
	private void updateCustomerInSession(Customer customer){
		getSession().setAttribute("customer", customer);
	}
	
	private Customer getCustomerFromSession(){
		return (Customer) getSession().getAttribute("customer");
	}
}
