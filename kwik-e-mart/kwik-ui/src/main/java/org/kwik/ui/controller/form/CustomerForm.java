package org.kwik.ui.controller.form;

import java.util.Date;

import org.kwik.customer.Credential;
import org.kwik.customer.Customer;
import org.kwik.customer.CustomerPhone;
import org.kwik.customer.Gender;

/**
 * @author Renan Scarela - 13/08/2013
 *
 */
public class CustomerForm {
	
	private Long id;
	
	private String name;
	private String surname;
	private String socialId;
	private Date birthDate;
	private Gender gender;
	
	private String residential;
	private String comercial;
	private String cellphone;
	
	private String email;
	private String password;
	
	public Customer getCustomer(){
		CustomerPhone customerPhone = new CustomerPhone(residential, comercial, cellphone);
		Customer customer = new Customer(id, name, surname, socialId, birthDate, gender, customerPhone);
		return customer;
	}
	
	public Credential getCredential(){
		return new Credential(email, password);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public void setSocialId(String socialId) {
		this.socialId = socialId;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public void setResidential(String residential) {
		this.residential = residential;
	}
	public void setComercial(String comercial) {
		this.comercial = comercial;
	}
	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
