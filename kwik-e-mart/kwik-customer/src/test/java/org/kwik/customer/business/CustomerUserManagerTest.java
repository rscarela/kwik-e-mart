package org.kwik.customer.business;

import java.security.InvalidParameterException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kwik.customer.Credential;
import org.kwik.customer.Customer;
import org.kwik.customer.CustomerAddress;
import org.kwik.customer.CustomerPhone;
import org.kwik.customer.business.repository.interfaces.ICustomerRepository;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

/**
 * @author Renan Scarela - 15/08/2013
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CustomerUserManagerTest extends Mockito {
	
	@Mock private ICustomerRepository customerRepository;
	
	private CustomerUserManager customerUserManager;
	
	private Customer customer;
	private Credential credential;
	private CustomerAddress customerAddress;
	
	@Before
	public void init(){
		buildObjects();
		setupMocks();
		customerUserManager = new CustomerUserManager(customerRepository);
	}
	
	@Test
	public void mustPersistANewUser(){
		customerUserManager.setCustomer(customer).createNew(credential);
		Assert.assertTrue(customer.getId().equals(1L));
	}
	
	@Test(expected=InvalidParameterException.class)
	public void mustThrowAnExceptionWhenCustomerIsNull(){
		customer = null;
		customerUserManager.setCustomer(customer);
	}
	
	@Test(expected=InvalidParameterException.class)
	public void mustThrowAnExceptionWhenCredentialIsNull(){
		credential = null;
		customerUserManager.setCustomer(customer).createNew(credential);
	}
	
	@Test
	public void mustPersistAnExistingUser(){
		customer.setId(1L);
		customerUserManager.setCustomer(customer).save();
		Assert.assertTrue(customer.getId().equals(1L));
	}
	
	@Test(expected=InvalidParameterException.class)
	public void mustThrowAnExceptionIfUsersIdIsNull(){
		customer.setId(null);
		customerUserManager.setCustomer(customer).save();
	}
	
	@Test
	public void mustGetCustomersAddress(){
		customer.setId(1L);
		customerUserManager.setCustomer(customer).getAddress();
		Assert.assertNotNull(customerAddress.getCustomer());
	}
	
	@Test(expected=InvalidParameterException.class)
	public void mustThrowAnExceptionWhenTryingGettingTheAddressFromAnUnexistingCustomer(){
		customer.setId(null);
		customerUserManager.setCustomer(customer).getAddress();
	}
	
	private void buildObjects() {
		customer = new Customer();
		customer.setCustomerPhone(new CustomerPhone());
		credential = new Credential("email", "password");
		customerAddress = new CustomerAddress();
	}
	
	private void setupMocks(){
		this.setupCustomerPersistMethod();
		this.setupCustomerPhonePersistMethod();
		this.setupCredentialPersistMethod();
		this.setupGetAddressMethod();
	}
	
	private void setupCustomerPersistMethod(){
		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) throws Throwable {
				customer.setId(1L);
				return null;
			}
		}).when(customerRepository).persist(customer);
	}
	
	private void setupCustomerPhonePersistMethod(){
		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) throws Throwable {
				customer.getCustomerPhone().creteNewRowOfPhonesFor(customer);
				return null;
			}
		}).when(customerRepository).persist(customer.getCustomerPhone());
	}
	
	private void setupCredentialPersistMethod(){
		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) throws Throwable {
				credential.setCustomer(customer);
				return null;
			}
		}).when(customerRepository).persist(credential);
	}
	
	private void setupGetAddressMethod(){
		doAnswer(new Answer<Void>() {
			public Void answer(InvocationOnMock invocation) throws Throwable {
				customer.setId(1L);
				customerAddress.setCustomer(customer);
				return null;
			}
		}).when(customerRepository).getCustomerAddressBy(customer);
	}
	
}
