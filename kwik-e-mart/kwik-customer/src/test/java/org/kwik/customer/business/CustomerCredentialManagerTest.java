package org.kwik.customer.business;

import java.security.InvalidParameterException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kwik.customer.Credential;
import org.kwik.customer.Customer;
import org.kwik.customer.business.interfaces.ICustomerCredentialManager;
import org.kwik.customer.business.repository.interfaces.ICustomerRepository;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * @author Renan Scarela - 16/08/2013
 *
 */

@RunWith(MockitoJUnitRunner.class)
public class CustomerCredentialManagerTest extends Mockito{

	@Mock private ICustomerRepository customerRepository;
	
	private ICustomerCredentialManager customerCredentialManager;
	
	private Credential credential;
	private Customer customer;
	
	@Before
	public void init(){
		buildObjects();
		setupGetCustomerMethod();
		customerCredentialManager = new CustomerCredentialManager(customerRepository);
	}
	
	private void buildObjects() {
		credential = new Credential("email", "password");
		customer = new Customer();
	}

	@Test(expected=InvalidParameterException.class)
	public void mustThrowAnExceptionWhenCredentialIsNull(){
		credential = null;
		customerCredentialManager.setCredential(credential);
	}
	
	@Test(expected=InvalidParameterException.class)
	public void mustThrowAnExceptionWhenCredentialIsInvalid(){
		customerCredentialManager.setCredential(new Credential(null, "a"));
	}
	
	@Test
	public void mustGetCustomerWithTheCorrectCredential(){
		customerCredentialManager.setCredential(credential).getCustomer();
		Assert.assertTrue(customer.getId().equals(1L));
	}
	
	@Test(expected=InvalidParameterException.class)
	public void mustMustThrowAnExceptionWhenTryingToGetCustomerWhoDoesntHaveCredential(){
		customerCredentialManager.setCredential(new Credential("wrong", "wrong")).getCustomer();
	}
	
	@Test
	public void mustReturnTrueWhenCredentialIsCorrect(){
		Assert.assertTrue(customerCredentialManager.setCredential(credential).doesExists());
	}
	
	@Test
	public void mustReturnFalseWhenCredentialIsincorrect(){
		Assert.assertFalse(customerCredentialManager.setCredential(new Credential("wrong", "wrong")).doesExists());
	}
	
	private void setupGetCustomerMethod(){
		customer.setId(1L);
		credential.setCustomer(customer);
		doReturn(credential).when(customerRepository).getCustomerCredentialWith(credential);
	}
	
}
