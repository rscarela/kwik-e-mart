package org.kwik.customer.business;

import org.kwik.customer.Credential;
import org.kwik.customer.Customer;
import org.kwik.customer.business.interfaces.ICustomerCredentialManager;
import org.kwik.customer.business.interfaces.ICustomerManager;
import org.kwik.customer.business.interfaces.ICustomerUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Renan Scarela - 11/08/2013
 *
 */

@Service
@Transactional(propagation=Propagation.REQUIRED)
public class CustomerManager implements ICustomerManager {
	
	private ICustomerUserManager customerUserManager;
	private ICustomerCredentialManager customerCredentialManager;
	
	@Autowired
	public CustomerManager(ICustomerUserManager customerUserManager,
			ICustomerCredentialManager customerCredentialManager) {
		this.customerUserManager = customerUserManager;
		this.customerCredentialManager = customerCredentialManager;
	}

	@Override
	public ICustomerUserManager forCustomer(Customer customer){
		return customerUserManager.setCustomer(customer);
	}
	
	@Override
	public ICustomerCredentialManager forCredential(Credential credential){
		return customerCredentialManager.setCredential(credential);
	}
	
}
