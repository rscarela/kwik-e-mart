package org.kwik.customer.business.repository;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.kwik.customer.Credential;
import org.kwik.customer.Customer;
import org.kwik.customer.CustomerAddress;
import org.kwik.customer.CustomerPhone;
import org.kwik.customer.business.repository.interfaces.ICustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Renan Scarela - 11/08/2013
 *
 */

@Repository
public class CustomerDAO implements ICustomerRepository {
	
	@Autowired private SessionFactory sessionFactory;

	private Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void persist(Customer customer){
		getSession().saveOrUpdate(customer);
	}
	
	@Override
	public void persist(Credential credential){
		getSession().saveOrUpdate(credential);
	}
	
	@Override
	public void persist(CustomerPhone customerPhone){
		getSession().saveOrUpdate(customerPhone);
	}

	@Override
	public Credential getCustomerCredentialWith(Credential credential) {
		Query q = getSession()
					.createQuery("from Credential credential " +
								"where credential.email = :email " +
								"and credential.password = :password")
					.setParameter("email", credential.getEmail())
					.setParameter("password", credential.getPassword());
		
		return (Credential) q.uniqueResult();
	}

	@Override
	public void persist(CustomerAddress customerAddress) {
		getSession().saveOrUpdate(customerAddress);
	}
	
	@Override
	public CustomerAddress getCustomerAddressBy(Customer customer){
		Query q = getSession()
				.createQuery("from CustomerAddress customerAddress " +
							"where customerAddress.customer.id = :customerId")
				.setParameter("customerId", customer.getId());
		
		return (CustomerAddress) q.uniqueResult();
	}
	
	@Override
	public Credential getCustomerCredentialBy(String email) {
		Query q = getSession()
					.createQuery("from Credential credential " +
								"where credential.email = :email")
					.setParameter("email", email);
		
		return (Credential) q.uniqueResult();
	}
	
}
