package org.kwik.customer.util.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JsonHttpConsumer {

	URL url;
	HttpURLConnection connection;
	String result;
	
	
	public JsonHttpConsumer(URL url) {
		this.url = url;
	}

	public JsonHttpConsumer doGet() throws IOException{
		setupConnection();
		getResponseContent();
		closeConnection();
		return this;
	}
	
	public HttpConsumerResult getResult(){
		return new HttpConsumerResult(result);
	}
	
	private void setupConnection() throws IOException{
		connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Accept", "application/json");
	}

	private void getResponseContent() throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
		
		String result = "";
		String output;
		while ((output = br.readLine()) != null) {
			result += output;
		}
		
		this.result = result;
	}
	
	private void closeConnection(){
		connection.disconnect();
	}
	
}
