package org.kwik.customer.business.repository.interfaces;

import org.kwik.customer.Credential;
import org.kwik.customer.Customer;
import org.kwik.customer.CustomerAddress;
import org.kwik.customer.CustomerPhone;

/**
 * @author Renan Scarela - 11/08/2013
 *
 */
public interface ICustomerRepository {

	void persist(Customer customer);

	void persist(Credential credential);

	void persist(CustomerPhone customerPhone);

	Credential getCustomerCredentialWith(Credential credential);

	void persist(CustomerAddress customerAddress);

	CustomerAddress getCustomerAddressBy(Customer customer);

	Credential getCustomerCredentialBy(String email);

}
