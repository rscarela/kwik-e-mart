package org.kwik.customer;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="CUSTOMER")
public class Customer {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="NAME", nullable=false)
	private String name;
	
	@Column(name="SURNAME", nullable=false)
	private String surname;
	
	@Column(name="SOCIAL_ID", nullable=false, unique=true, updatable=false)
	private String socialId;
	
	@Column(name="BIRTH_DATE", nullable=false, updatable=false)
	private Date birthDate;
	
	@Enumerated
	@Column(name="GENDER")
	private Gender gender;
	
	@OneToOne(mappedBy="customer")
	private CustomerPhone customerPhone;
	
	@OneToOne(mappedBy="customer")
	private CustomerAddress customerAddress;
	
	public Customer() {}
	
	public Customer(Long id, String name, String surname, String socialId,
			Date birthDate, Gender gender, CustomerPhone customerPhone) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.socialId = socialId;
		this.birthDate = birthDate;
		this.gender = gender;
		this.customerPhone = customerPhone;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSocialId() {
		return socialId;
	}

	public void setSocialId(String socialId) {
		this.socialId = socialId;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public CustomerPhone getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(CustomerPhone customerPhone) {
		this.customerPhone = customerPhone;
	}
	
	public void createNewRowOfPhones(){
		customerPhone.creteNewRowOfPhonesFor(this);
	}

	public CustomerAddress getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(CustomerAddress customerAddress) {
		this.customerAddress = customerAddress;
	}
	
	public void defineAddress(){
		customerAddress.setAddressFor(this);
	}
}