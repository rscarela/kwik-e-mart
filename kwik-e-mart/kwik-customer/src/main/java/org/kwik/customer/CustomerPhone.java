package org.kwik.customer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="CUSTOMER_PHONE")
public class CustomerPhone {
	
	@Id
	@Column(name="CUSTOMER_ID", nullable=false)
	private Long customerId;
	
	@ManyToOne
	@JoinColumn(name="CUSTOMER_ID", updatable=false, insertable=false)
	private Customer customer;
	
	@Column(name="RESIDENTIAL_NUMBER")
	private String residential;
	
	@Column(name="COMERCIAL_NUMBER")
	private String comercial;
	
	@Column(name="CELLPHONE_NUMBER")
	private String cellphone;
	
	public CustomerPhone() {}
	
	public CustomerPhone(String residential, String comercial, String cellphone) {
		this.residential = residential;
		this.comercial = comercial;
		this.cellphone = cellphone;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getResidential() {
		return residential;
	}

	public void setResidential(String residential) {
		this.residential = residential;
	}

	public String getComercial() {
		return comercial;
	}

	public void setComercial(String comercial) {
		this.comercial = comercial;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}
	
	public void creteNewRowOfPhonesFor(Customer customer){
		customerId = customer.getId();
	}
}
