package org.kwik.customer.util.http;

import java.net.MalformedURLException;
import java.net.URL;

public class WebserviceConsumer {
	
	public JsonHttpConsumer consume(String url) throws MalformedURLException{
		return new JsonHttpConsumer(new URL(url));
	}
	
}
