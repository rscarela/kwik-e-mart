package org.kwik.customer.util.digester;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Renan Scarela - 26/09/2012
 */

/**
 * Classe respons�vel por realiza��o de hash.
 * 
 * Hashes suportados:
 * MD5;
 * SHA-1;
 * SHA-256;
 * SHA-512
 */
public class Digester {
	
	private MessageDigest cypher;
	
	public Digester(HashType hashType){
		try {
			cypher = MessageDigest.getInstance(hashType.getType());
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Type does not exists");
		}
	}
	
	public String digest(String string){
		return toHexaString(cypher.digest(string.getBytes()));
	}
	
	private static String toHexaString(byte[] bytes) {
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			int lowLevel = ((bytes[i] >> 4) & 0xf) << 4;
			int highLevel = bytes[i] & 0xf;
			if (lowLevel == 0) s.append('0');
			s.append(Integer.toHexString(lowLevel | highLevel));
		}
		return s.toString();
	}
	
}