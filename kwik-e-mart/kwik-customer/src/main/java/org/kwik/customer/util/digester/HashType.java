package org.kwik.customer.util.digester;

public enum HashType {
	
	MD5("MD5"),
	SHA1("SHA-1"),
	SHA256("SHA-256"),
	SHA512("SHA-512");
	
	private String type;
	
	private HashType(String type){
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
}
