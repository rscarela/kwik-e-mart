package org.kwik.customer;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="CUSTOMER_ADDRESS")
public class CustomerAddress implements Serializable {
	
	private static final long serialVersionUID = -4426249579765545235L;

	@Id
	@Column(name="CUSTOMER_ID", nullable=false)
	private Long customerId;
	
	@OneToOne
	@JoinColumn(name="CUSTOMER_ID", insertable=false, updatable=false)
	private Customer customer;
	
	@Column(name="STREET")
	private String street;
	
	@Column(name="NEIGHBORHOOD", nullable=false)
	private String neighborhood;
	
	@Column(name="ZIPCODE", nullable=false)
	private String zipcode;
	
	@Column(name="NUMBER", nullable=false)
	private String number;
	
	@Column(name="NOTE")
	private String note;
	
	@Column(name="CITY")
	private String city;
	
	@Column(name="STATE")
	private String state;
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	public void setAddressFor(Customer customer){
		customerId = customer.getId();
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}