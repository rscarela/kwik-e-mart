package org.kwik.customer.business.interfaces;

import org.kwik.customer.Credential;
import org.kwik.customer.Customer;

/**
 * @author Renan Scarela - 11/08/2013
 *
 */
public interface ICustomerManager {

	ICustomerUserManager forCustomer(Customer customer);

	ICustomerCredentialManager forCredential(Credential credential);

}
