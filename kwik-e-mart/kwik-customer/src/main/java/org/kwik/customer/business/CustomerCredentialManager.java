package org.kwik.customer.business;

import java.security.InvalidParameterException;

import org.kwik.customer.Credential;
import org.kwik.customer.Customer;
import org.kwik.customer.business.interfaces.ICustomerCredentialManager;
import org.kwik.customer.business.repository.interfaces.ICustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Renan Scarela - 12/08/2013
 *
 */

@Component
@Transactional
public class CustomerCredentialManager implements ICustomerCredentialManager {
	
	private ICustomerRepository customerRepository;
	private Credential credential;
	
	@Autowired
	public CustomerCredentialManager(ICustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}
	
	@Override
	public ICustomerCredentialManager setCredential(Credential credential) {
		if(credential == null || credential.hasInvalidParameter())
			throw new InvalidParameterException();
		
		this.credential = credential;
		return this;
	}

	@Override
	public Customer getCustomer(){
		credential = this.customerRepository.getCustomerCredentialWith(credential);
		
		if(credential == null) throw new InvalidParameterException();
		
		return credential.getCustomer();
	}
	
	@Override
	public boolean doesExists(){
		credential = this.customerRepository.getCustomerCredentialWith(credential);
		if(credential == null) return false;
		return true;
	}
	
}
