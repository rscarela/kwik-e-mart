package org.kwik.customer;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.kwik.customer.util.digester.Digester;
import org.kwik.customer.util.digester.HashType;
import org.springframework.util.StringUtils;


@Entity
@Table(name="CUSTOMER_CREDENTIAL")
public class Credential implements Serializable {
	
	private static final long serialVersionUID = -289944342360570062L;

	@Id
	@Column(name="CUSTOMER_ID", nullable=false)
	private Long customerId;
	
	@Column(name="EMAIL", nullable=false, updatable=false, unique=true)
	private String email;
	
	@Column(name="PASSWORD", nullable=false)
	private String password;
	
	@OneToOne
	@JoinColumn(name="CUSTOMER_ID", updatable=false, insertable=false)
	private Customer customer;
	
	public Credential() {}
	
	public Credential(String email, String password) {
		this.email = email;
		this.password = new Digester(HashType.MD5).digest(password);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = new Digester(HashType.MD5).digest(password);
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public void createNewCredentialFor(Customer customer){
		customerId = customer.getId();
	}
	
	public boolean hasInvalidParameter() {
		if(StringUtils.isEmpty(getEmail())
				|| StringUtils.isEmpty(getPassword())) return true;
		return false;
	}
	
}
