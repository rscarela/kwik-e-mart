package org.kwik.customer.business.interfaces;

import org.kwik.customer.Credential;
import org.kwik.customer.Customer;
import org.kwik.customer.CustomerAddress;

/**
 * @author Renan Scarela - 11/08/2013
 *
 */
public interface ICustomerUserManager {

	ICustomerUserManager setCustomer(Customer customer);

	void createNew(Credential credential);

	void save();

	void updateAddress(CustomerAddress customerAddress);

	CustomerAddress getAddress();

}
