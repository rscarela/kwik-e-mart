package org.kwik.customer.business.interfaces;

import org.kwik.customer.Credential;
import org.kwik.customer.Customer;

/**
 * @author Renan Scarela - 12/08/2013
 *
 */
public interface ICustomerCredentialManager {

	ICustomerCredentialManager setCredential(Credential credential);

	Customer getCustomer();

	boolean doesExists();

}
