package org.kwik.customer.util.http;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class HttpConsumerResult {
	
	private String json;
	
	public HttpConsumerResult(String json) {
		this.json = json;
	}
	
	public String asPlainText(){
		return json;
	}
	
	public Object asObject(Class<?> clazz) throws JsonParseException, JsonMappingException, IOException{
		return new ObjectMapper().readValue(json, clazz);
	}
	
}
