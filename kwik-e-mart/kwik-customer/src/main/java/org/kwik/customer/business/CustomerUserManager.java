package org.kwik.customer.business;

import java.security.InvalidParameterException;

import org.kwik.customer.Credential;
import org.kwik.customer.Customer;
import org.kwik.customer.CustomerAddress;
import org.kwik.customer.business.interfaces.ICustomerUserManager;
import org.kwik.customer.business.repository.interfaces.ICustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Renan Scarela - 11/08/2013
 *
 */

@Component
@Transactional
public class CustomerUserManager implements ICustomerUserManager {
	
	private ICustomerRepository customerRepository;
	private Customer customer;
	
	@Autowired
	public CustomerUserManager(ICustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@Override
	public ICustomerUserManager setCustomer(Customer customer) {
		if(customer == null) throw new InvalidParameterException();
		
		this.customer = customer;
		return this;
	}
	
	@Override
	public void createNew(Credential credential){
		if(credential == null) throw new InvalidParameterException();
		
		customerRepository.persist(customer);
		savePhoneContactsRow();
		createNewUsersCredential(credential);
	}
	
	@Override
	public void save(){
		if(customer.getId() == null) throw new InvalidParameterException();
		
		customerRepository.persist(customer);
		savePhoneContactsRow();
	}
	
	@Override
	public void updateAddress(CustomerAddress customerAddress){
		if(customerAddress == null) throw new InvalidParameterException();
		
		customer.setCustomerAddress(customerAddress);
		customer.defineAddress();
		customerRepository.persist(customer.getCustomerAddress());
	}
	
	@Override
	public CustomerAddress getAddress(){
		if(customer.getId() == null) throw new InvalidParameterException();
		
		return customerRepository.getCustomerAddressBy(customer);
	}
	
	private void savePhoneContactsRow(){
		if(customer.getCustomerPhone() == null) return;
		
		customer.createNewRowOfPhones();
		customerRepository.persist(customer.getCustomerPhone());
	}
	
	private void createNewUsersCredential(Credential credential){
		credential.createNewCredentialFor(customer);
		customerRepository.persist(credential);
	}
}
